<?php get_header() ?>
	<div class="ordering">
		<h1>Оформление заказа</h1>
		<div class="inner-l">
			<form action="https://www.liqpay.com/?do=clickNbuy" method="POST" id="checkout-form">
				<div class="form-box-short underline">

					<div class="box-title">
						Способ доставки
					</div>
					<div class="row">
						<input type="radio" id="courier" data-label="Метод Доставки" value="Курьер" name="CheckoutForm[delivery-method]" class="required"/>
						<label for="courier" class="up">Курьер</label>
					</div>
					<div class="row">
						<input type="radio" id="ukrpost" data-label="Метод Доставки" value="Укрпочта" name="CheckoutForm[delivery-method]" class="required"/>
						<label for="ukrpost" class="up">Укрпочта</label>
					</div>
					<div class="row">
						<input type="radio" id="newpost" data-label="Метод Доставки" value="Доставка Новой Почтой" name="CheckoutForm[delivery-method]" class="required"/>
						<label for="newpost" class="up">Доставка Новой Почтой</label>
					</div>
					<div class="row">
						<input type="radio" id="dhl" data-label="Метод Доставки" value="Доставка DHL" name="CheckoutForm[delivery-method]" class="required"/>
						<label for="dhl" class="up">Доставка DHL</label>
					</div>
				</div>

				<div class="form-box underline">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="firstname">Имя:</label>
						<input type="text" id="firstname" data-label="Имя" name="CheckoutForm[name]" class="required"/>
					</div>
					<div class="row">
						<label for="lastname">Фамилия:</label>
						<input type="text" id="lastname" data-label="Фамилия" name="CheckoutForm[surname]" class="required"/>
					</div>
					<div class="row">
						<label for="patronymic">Отчество:</label>
						<input type="text" id="patronymic" data-label="Отчество" name="CheckoutForm[patronymic]" class="required"/>
					</div>
					<div class="row">
						<label for="phone">Телефон:</label>
						<input type="text" id="phone" name="CheckoutForm[phone]" data-label="Телефон" class="required"/>
					</div>
					<div class="row">
						<label for="phone">Email:</label>
						<input type="text" id="phone" name="CheckoutForm[email]" data-label="Телефон" class="required"/>
					</div>

				</div>

				<div class="form-box">
					<div class="box-title">
						Информация о получателе
					</div>
					<div class="row">
						<label for="city">Населенный пункт:</label>
						<input type="text" title="Населенный пункт:" data-label="Город"  id="city" name="CheckoutForm[city]" class="required"/>
					</div>
					<div class="row">
						<label for="vilage">Улица:</label>
						<input type="text" id="vilage" name="CheckoutForm[street]" data-label="Улица" class="required"/>
					</div>
					<div class="row">
						<label for="house">Дом:</label>
						<input type="text" id="house" name="CheckoutForm[house]" data-label="Дом" class="required"/>
					</div>
					<div class="row">
						<label for="flat">Квартира:</label>
						<input type="text" id="flat" name="CheckoutForm[flat]" data-label="Квартира" class="required"/>
					</div>
				</div>
				<?php
					/**
					 * Cart Page
					 *
					 * @author 		WooThemes
					 * @package 	WooCommerce/Templates
					 * @version     1.6.4
					 */

					if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

					global $woocommerce;

					$woocommerce->show_messages();
				?>
				<?php
				//<----- @TODO: сделать кастомными настройки для merchant_id,  merchant_sign, default_phone
				$merchant_id = synved_option_get('options', 'merchant_id');
				$merc_sign	 = synved_option_get('options', 'merc_sign');
				$default_phone = synved_option_get('options', 'default_phone');
				//------>

				$order_id =  'uniq_id';
				$total_amount =  $woocommerce->cart->cart_contents_total;
				$xml="<request>
					<version>1.2</version>
					<merchant_id>i6341646097</merchant_id>
					<result_url>".$_SERVER['HTTP_HOST'] . "/processing?liqpay=true</result_url>
					<server_url>".$_SERVER['HTTP_HOST'] . "/processing?liqpay=true</server_url>
					<order_id>$order_id</order_id>
					<amount>$total_amount</amount>
					<currency>UAH</currency>
					<description>Comment</description>
					<default_phone></default_phone>
					<pay_way>card</pay_way>
					<goods_id>1234</goods_id>
					<exp_time>24</exp_time>
				</request>";
				$sign=base64_encode(sha1($merc_sign.$xml.$merc_sign,1));
				$xml_encoded=base64_encode($xml);
				?>

				<input type="hidden" name="operation_xml" value="<?php echo $xml_encoded?>" />
				<input type="hidden" name="signature" value="<?php echo $sign?>" />
				<input type="submit" id="order-submit" value="Оформить заказ"/>
			</form>
			<div id="errors"></div>
		</div>
		<div class="inner-r">


		<?php do_action( 'woocommerce_before_cart' ); ?>

		<form action="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" method="post">

			<?php do_action( 'woocommerce_before_cart_table' ); ?>

			<!--table class="shop_table cart" cellspacing="0">
				<thead>
				<tr>
					<th class="product-remove">&nbsp;</th>
					<th class="product-thumbnail">&nbsp;</th>
					<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
					<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
					<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
					<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
				</tr>
				</thead>
				<tbody-->
				<?php do_action( 'woocommerce_before_cart_contents' ); ?>

				<?php
				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
						$_product = $values['data'];
						if ( $_product->exists() && $values['quantity'] > 0 ) {
							?>
							<div class="order-item">
								<table>
									<tr>
										<td>
											<div class="item-foto">
												<!-- The thumbnail -->
												<?php
												$thumbnail = apply_filters( 'woocommerce_in_cart_product_thumbnail', $_product->get_image(), $values, $cart_item_key );

												if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )
													echo $thumbnail;
												else
													printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), $thumbnail );
												?>
											</div>
										</td>
										<td>
											<div class="item-descr">
												<div class="item-title">
													<a href="#">
														<!-- Product Name -->
														<?php
														if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )

															echo apply_filters( 'woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key );
														else
																		printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), apply_filters('woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key ) );

														// Meta data
														//echo $woocommerce->cart->get_item_data( $values );

														// Backorder notification
														if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $values['quantity'] ) )
															echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
														?>


														- <?php echo $woocommerce->cart->get_item_data( $values, true );?></a>
												</div>
											<?php $size = $woocommerce->cart->get_item_data( $values, true )?>
											<?php if(!empty($size)) {?>
												<span class="item-size">
													<?php echo $size?>
												</span>
											<?}?>
												<?php
												if ( $_product->is_sold_individually() ) {
													$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
												} else {

													$step	= apply_filters( 'woocommerce_quantity_input_step', '1', $_product );
													$min 	= apply_filters( 'woocommerce_quantity_input_min', '', $_product );
													$max 	= apply_filters( 'woocommerce_quantity_input_max', $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(), $_product );

													//$product_quantity = sprintf( '<div class="quantity"><input type="number" name="cart[%s][qty]" step="%s" min="%s" max="%s" value="%s" size="4" title="' . _x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ) . '" class="input-text qty text" maxlength="12" /></div>', $cart_item_key, $step, $min, $max, esc_attr( $values['quantity'] ) );
													echo '<span class="item-num">';
													echo '<select name="num">';

													//for( $i = 1; $i < $values['quantity']; $i++ ){
													//	echo '<option value="'.$i .'">' .$i . ' шт'. '</option>';

													//}
													echo '<option selected="selected" value="'. $values['quantity'] .'">' .$values['quantity'] . ' шт'. '</option>';
													echo '</select>';
													echo '</span>';
												}

												echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
												?>


											<span class="item-prize">

												<?php
													$product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();
													echo apply_filters('woocommerce_cart_item_price_html', woocommerce_price( $product_price * $values['quantity']), $values, $cart_item_key );
												?>
											</span>
											</div>
										</td>
										<td>
											<!-- Remove from cart link -->
											<div class="close">
												<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a style="display: block; width: 25px; height: 25px" href="%s" class="remove" title="%s"></a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key ); ?>
											</div>
										</td>
									</tr>
								</table>
							</div>
						<?php
						}
					}
				}

				do_action( 'woocommerce_cart_contents' );
				?>
				<tr>
					<td colspan="6" class="actions">

						<?php if ( $woocommerce->cart->coupons_enabled() ) { ?>
							<div class="coupon">

								<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input name="coupon_code" class="input-text" id="coupon_code" value="" />
								<!--input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" /-->

								<?php do_action('woocommerce_cart_coupon'); ?>

							</div>
						<?php } ?>

						<!--input type="submit" class="button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" /-->
						<!--input type="submit" class="checkout-button button alt" name="proceed" value="<?php _e( 'Proceed to Checkout &rarr;', 'woocommerce' ); ?>" /-->

						<?php do_action('woocommerce_proceed_to_checkout'); ?>

						<?php $woocommerce->nonce_field('cart') ?>
					</td>
				</tr>

				<?php do_action( 'woocommerce_after_cart_contents' ); ?>
				<!--/tbody-->
			<!--/table-->

			<?php do_action( 'woocommerce_after_cart_table' ); ?>

		</form>

		<div class="cart-collaterals">

			<?php do_action('woocommerce_cart_collaterals'); ?>

			<?php woocommerce_cart_totals(); ?>

			<?php woocommerce_shipping_calculator(); ?>

		</div>

		<?php do_action( 'woocommerce_after_cart' ); ?>

			<div class="paiment-result">
				<p>
					Сумма к оплате:
					<?php
                    echo $woocommerce->cart->get_cart_subtotal();
					?>
				</p>
			</div>
		</div>
	</div>
<?php get_footer() ?>
<script>

$(function(){
	$('#order-submit').click(function(){
		var errors = 0;
		$('#errors').html('');
		$('.error').removeClass('error');
		$('.required').each(function(){
			var val = $(this).val();
			var name = $(this).attr('name');
			if ($(this).attr('type') == 'radio') {
				if($('input[name='+name+']:checked').length > 0) {
					val = $('input[name='+name+']:checked').val();
				} else {
					val = '';
				}
			}

			if ($(this).attr('type') == 'checkbox') {
				if($('input[name='+name+']').attr('checked') == 'checked') {
					val = "yes";
				} else {
					val = '';
					$(this).parent().addClass('error');
				}
			}

			if (val.length == 0) {
				var label = $(this).data('label')
				if(!label)
					label = name;

				$(this).addClass('error');
				$(this).parents('fieldset').addClass('error');
				errors++;
				var error = '<li>Заполните обязательное поле ' + label + '</li>';
				$('#errors').append(error);
			}
		});

		if (errors > 0){
			return false
		} else {
			var form = $('#checkout-form');
			$.ajax({
				'url' : '/processing',
				'data' : { 'CheckoutForm' : form.serialize() },
				'type' : 'POST',
				'dataType' : 'json',
				success : function(responce){
					if(responce === 'ok') {
						form.submit();
					} else {
						alert('Произошла какая-то ошибка, попробуйте ещё раз');
						return false;
					}
				}
			})
		}
		return false;
	});
})
</script>