<?php get_header() ?>
<div class="slider">
    <div class="camera_wrap camera_azure_skin" id="camera_wrap_1" style="height:538px">
        <?php query_posts('post_type=slide'); while(have_posts()): the_post();
        $image_id = get_post_thumbnail_id($post->ID);
        $url = wp_get_attachment_url( $image_id );
        $urlThumb = wp_get_attachment_image_src( $image_id, 'medium' );
        $urlThumb = $urlThumb[0];
        $model_image = synved_option_get('options', 'model_image');
        $model_desc = synved_option_get('options', 'model_description');
        ?>
        <div data-thumb="<?php echo $urlThumb ?>" data-src="<?php echo $url ?>">
            <div class="camera_caption fadeIn" style="visibility: hidden; opacity: 1;">
                <div>
                    <h1><?php the_title() ?></h1>
                    <h2><?php the_content() ?></h2>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div><!-- #camera_wrap_1 -->
</div>
<div class="gallery">
    <div class="title">Подбери свой стиль</div>
    <div class="gallery-row">
        <ul class="gallery-list">
            <li>
                <div class="gblock">
                    <a href="/order/" class="link"><?php if($model_image){?><img src="<?php echo $model_image; ?>"  width="100" height="100" alt=""/><?php } ?><span></span></a>
                </div>
                <div class="li-bottom"><a href="/order/">Создай свой стиль</a></div>
                <div class="description"><?php if($model_desc) echo $model_desc;?></div>
            </li>
            <?php include "content-product_cat.php"; ?>
        </ul>
    </div>
</div>
<div class="fraims">
    <div class="title">Пообщайся с нами</div>
    <div class="fraim-right">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-comments" data-href="<?php bloginfo( 'url' ); ?>" data-width="400"></div>
    </div>
    <div class="fraim-left">
        <!-- Put this script tag to the <head> of your page -->
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?98"></script>

        <script type="text/javascript">
            VK.init({apiId: <?php echo synved_option_get('options', 'vkid') ?>, onlyWidgets: true});
        </script>

        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
            VK.Widgets.Comments("vk_comments", {limit: 10, width: "400", attach: "*"});
        </script>
    </div>
</div>
<div class="reviews">
    <?php $args = array( 'post_type' => 'reviews', 'posts_per_page' => 1, 'orderby' => 'rand' );
    $review = new WP_Query( $args );
    while ( $review->have_posts() ) : $review->the_post();
        $rev_image_id = get_post_thumbnail_id($post->ID);
        $rev_url = wp_get_attachment_url( $rev_image_id );
        $RevUrlThumb = wp_get_attachment_image_src( $rev_image_id);
        $RevUrlThumb = $RevUrlThumb[0];
        $rev_url = get_post_meta( $post->ID, 'url', true );?>
        <div class="rev-thumb"><img src="<?php echo $RevUrlThumb?>" /></div>
        <div class="entry-content">
            <?php
            the_content();
            if($rev_url != ''){?>
                <a href="<?php echo $rev_url[0]["url"]?>"><?php the_title();?></a>
            <?php } else {
                the_title();
            }?>
        </div>
    <?php endwhile;?>
    <a href="/reviews/" class="link">Читать все отзывы</a>
</div>
<?php get_footer() ?>