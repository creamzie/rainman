<?php global $post, $product, $woocommerce, $re_wcvt_options; ?>
<div class="gallery-detail">
    <?php do_action( 'woocommerce_before_single_product' );?>
    <div class="gd-gallery">
        <div class="preview">
            <ul>
               <?php $attachment_ids = $product->get_gallery_attachment_ids();
                if ( $attachment_ids ) {
                    $loop = 0;
                    $columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
                    foreach ( $attachment_ids as $attachment_id ) {
                        $classes = array( 'zoom' );
                        if ( $loop == 0 || $loop % $columns == 0 )
                            $classes[] = 'first';
                        if ( ( $loop + 1 ) % $columns == 0 )
                            $classes[] = 'last';
                        $image_link = wp_get_attachment_url( $attachment_id );
                        if ( ! $image_link )
                            continue;
                        $image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
                        $image_class = esc_attr( implode( ' ', $classes ) );
                        $image_title = esc_attr( get_the_title( $attachment_id ) );
                        echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li><a href="%s" class="%s"  rel="prettyPhoto[product-gallery]">%s</a></li>', $image_link, $image_class, $image ), $attachment_id, $post->ID, $image_class );
                        $loop++;
                    }
                }
                ?>
            </ul>
        </div>
        <div class="foto">
            <?php
            if ( has_post_thumbnail() ) {
                $image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
                $image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
                $image       		= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
                    'title' => $image_title,
                    'data-zoom-image' => $image_link
                ) );
                $attachment_count   = count( $product->get_gallery_attachment_ids() );
                if ( $attachment_count > 0 ) {
                    $gallery = '[product-gallery]';
                } else {
                    $gallery = '';
                }
                echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '%s', $image ), $post->ID );
            } else {
                echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', woocommerce_placeholder_img_src() ), $post->ID );

            }
            ?>
        </div>
    </div>
    <div class="gd-description">
        <h4><?php the_title(); ?></h4>
        <div class="gd-price"><?php echo $product->get_price().' грн.'; ?></div>
        <div class="gd-size">
        <?php if($product->product_type == 'variable'):?>
                <p>
                    Выберите свой размер:
                    <?php $available_variations = $product->get_available_variations();
                    foreach ($available_variations as $prod_variation) :
                        $post_id = $prod_variation['variation_id'];
                        $post_object = get_post($post_id);
                        foreach ($prod_variation['attributes'] as $attr_name => $attr_value) :
                            // Get the correct variation values
                            if (strpos($attr_name, '_pa_')){ // variation is a pre-definted attribute
                                $attr_name = substr($attr_name, 10);
                                $attr = get_term_by('slug', $attr_value, $attr_name);
                                $attr_value = $attr->name;
                            }
                            echo '<a href="#" rel="'.$post_id.'-'.$attr_value.'">'.$attr_value.'</a>';
                        endforeach;?>
                    <?php endforeach;?>
                </p>
            <?php endif;?>
            <?php if($product->product_type == 'variable'):?>
                <ul id="errors"></ul>
                <form action="<?php echo do_shortcode('[add_to_cart_url id="'.$product->id.'"]'); ?>" class="variations_form cart" method="post" enctype="multipart/form-data" data-product_id="<?php echo $product->id; ?>">
                    <input type="hidden" class="input-text qty text" title="Qty" value="1" name="quantity" step="1">
                    <input type="hidden" name="variation_id" value="<?php //echo $post_id; ?>">
                    <input type="hidden" name="<?php echo 'attribute_%d1%80%d0%b0%d0%b7%d0%bc%d0%b5%d1%80'; ?>" value="<?php //echo $attr_value ;?>">
                </form>
            <?php elseif($product->product_type == 'simple'): ?>
                <form enctype="multipart/form-data" method="post" class="cart">
                    <input type="hidden" class="input-text qty text" title="Qty" value="1" name="quantity" step="1">
                    <input type="hidden" value="<?php echo $product->id; ?>" name="add-to-cart">
                </form>
            <?php endif;?>
        </div>
        <div class="gd-info">
            <?php the_content(); ?>
        </div>
        <div class="gd-basket">
            <a href="#">
                В корзину
            </a>
        </div>
    </div>
    <div class="clear"></div>
    <div class="gd-delivery">
        <h5>Условия доставки</h5>
        <?php
            $p = get_page(252);
            echo apply_filters('the_content', $p->post_content);
        ?>
        <a href="/conditions/">Подробнее</a>
    </div>
</div>
<?php //do_action( 'woocommerce_after_single_product_summary' );?>

<?php if($product->product_type == 'variable'):?>
    <script>
        jQuery(document).ready(function() {
            $(".attachment-shop_single").elevateZoom({
                zoomType				: "inner",
                cursor: "crosshair"
            });

            $('input[name="variation_id"]').val('');
            $('input[name="attribute_%d1%80%d0%b0%d0%b7%d0%bc%d0%b5%d1%80"]').val('');

            $('.gd-basket').click(function(){
               var error_container = $('#errors');
               var form = $('.variations_form');
               var size = $('input[name="variation_id"]').val();
               var attr = $('input[name="attribute_%d1%80%d0%b0%d0%b7%d0%bc%d0%b5%d1%80"]').val();

               if(size == '' || attr == ''){
                   error_container.html('<li>Укажите размер</li>');
               } else {
                   form.submit();
               }
               return false;
            });
            $('.gd-size a').click(function(){
                var error_container = $('#errors');
                var data = $(this).attr('rel');
                var variations = data.split('-');
                var size = $('input[name="variation_id"]');
                var attr = $('input[name="attribute_%d1%80%d0%b0%d0%b7%d0%bc%d0%b5%d1%80"]');


                size.val(variations[0]);
                attr.val(variations[1]);
                error_container.html('');
                $('.gd-size a').css('text-decoration','underline');
                $('.gd-size a').css('color','#1253B4');
                $(this).css('text-decoration','none');
                $(this).css('color','#000000');
                return false;
            });
        });
    </script>
<?php elseif($product->product_type == 'simple'): ?>
    <script>
        jQuery(document).ready(function() {
            $(".attachment-shop_single").elevateZoom({
                zoomType				: "inner",
                cursor: "crosshair"
            });

            $('.gd-basket').click(function(){
                var form = $('.cart');

                form.submit();
                return false;
            });
        });
    </script>
<?php endif;?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/prettyPhoto/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/prettyPhoto/jquery.prettyPhoto.init.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/prettyPhoto.css" type="text/css"/>