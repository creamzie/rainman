<?php
/**
 * Pagination - Show numbered pagination for catalog pages.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wp_query;

if ( $wp_query->max_num_pages <= 1 )
    return;
?>
<div class="pager">
    <ul>
        <?php
        $pg = paginate_links( apply_filters( 'woocommerce_pagination_args', array(
            'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
            'format' 		=> '',
            'current' 		=> max( 1, get_query_var('paged') ),
            'total' 		=> $wp_query->max_num_pages,
            'prev_text' 	=> 'Предыдущая',
            'next_text' 	=> 'Следующая',
            'type'			=> 'array',
            'end_size'		=> 3,
            'mid_size'		=> 3
        ) ) );
        if (mb_strpos($pg[0],'Предыдущая')) echo '<li class="prev">'.$pg[0].'</li>';
        echo '<li class="after-prev">Страница '.max( 1, get_query_var('paged') ).' из '.$wp_query->max_num_pages.'</li>';
        if (mb_strpos(end($pg),'Следующая'))echo '<li class="next">'.end($pg).'</li>';
        ?>
    </ul>
</div>