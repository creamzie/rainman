<?php
    $prod_categories = get_terms( 'product_cat', array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'hide_empty' => 0
    ));
    foreach( $prod_categories as $prod_cat ) :
        $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
        $cat_thumb_url = wp_get_attachment_thumb_url( $cat_thumb_id );
?>
<li>
    <div class="gblock">
        <a href="<?php echo get_term_link( $prod_cat->slug, 'product_cat' ); ?>" class="link"><img src="<?php echo $cat_thumb_url; ?>"  width="100" height="100" alt="<?php echo $prod_cat->name; ?>"/><span></span></a>
    </div>
    <div class="li-bottom"><a href="<?php echo get_term_link( $prod_cat->slug, 'product_cat' ); ?>"><?php echo $prod_cat->name; ?></a></div>
    <div class="description"><?php echo $prod_cat->description; ?></div>
</li>
<?php endforeach; wp_reset_query(); ?>
